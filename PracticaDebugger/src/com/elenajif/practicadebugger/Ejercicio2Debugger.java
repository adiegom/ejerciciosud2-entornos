package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
			
			// En ningun momento la variable 'i' debe ser '=' 
			// que 0. 
			// Ello dara un fallo, puesto que
			// dividir entre 0 da 0 y por ello error en el programa.
			
			//-------------------------------------------------
			
			// Es por ello que para solucionarlo
			// hay que quitar el '='.
			// for(int i = numeroLeido; i > 0 ; i--)
			
			
			
		}
				
		lector.close();
	}

}
